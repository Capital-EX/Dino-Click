local out, width, height, cellsize, step_x, step_y, name
local format = string.format
namef = "%s_%d.tres"
tresf = 
[[
[gd_resource type="AtlasTexture" load_steps=2 format=1]

[ext_resource path="res://images/sprites.png" type="Texture" id=1]

[resource]

atlas = ExtResource( 1 )
region = Rect2( %d, %d, %d, %d )
margin = Rect2( 0, 0, 0, 0 )
]]
print("Hello")
width = tonumber(arg[1])
height = tonumber(arg[2])
cellsize = tonumber(arg[3])
name = arg[4]
local id = 0
-- print(format(tresf, 0, 0, cellsize, cellsize))
--[-[
for y = 0, height/cellsize - 1 do
	for x = 0, width/cellsize - 1 do
		id = id + 1
		local file = io.open(format(namef, name, id), "w")
		print(x * cellsize, y * cellsize)
		file:write(format(tresf, x * cellsize, y * cellsize, cellsize, cellsize))
		file:close()
	end
end
--]]
