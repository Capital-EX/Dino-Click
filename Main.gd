extends Node2D
export (bool) var dev_mode_on
onready var Dinos = get_node("Dinos")
const S = preload("res://scripts/SaveManger.gd")
const FloatAway = preload("res://scenes/FloatAway.tscn")
const Dino = preload("res://scenes/Dino.tscn")
const BigDecimal = preload("res://scripts/BigDecimal.gd")
const auto_save_rate = 10
const money_string = "%0.0f$"
const log_10 = log(10)
var time_since_save = 0

var old_money = 0
var spawn_thresh = 10
var spawn_acc = 0

# helper function to do recursive deep copy of
# data structers
# * does not cover nested arrays.


var stats
# On dict to rule them all!


onready var hud =\
	{ Money =\
		get_node("GUI/Money")
	, Egg =\
		get_node("GUI/Egg")
	, Shop =\
		get_node("GUI/Shop")
	, Upgrades =\
		get_node("GUI/Upgrades")
	, Stats =\
		get_node("GUI/Stats")
	, About =\
		get_node("GUI/About")
	, DeleteSave =\
		get_node("GUI/DeleteSave")
	, GUI =\
		get_node("GUI")
	}

func _process(delta):
	time_since_save += delta
	if time_since_save >= auto_save_rate:
		time_since_save = 0
		S.game_save_state(stats, hud.Shop.prices)
	
	stats.float_money += calculate(delta)
	if stats.float_money >= 1:
		stats.money += int(stats.float_money)
		stats.float_money -= int(stats.float_money)
	

		
	
	if old_money != stats.money:
		hud.Shop.check_for_unlocks(stats.money)
		hud.Money.set_text(money_string % stats.money)
		hud.Money.set_pos(Vector2(280 - hud.Money.get_size().x / 2, 0))
		var delta_money = stats.money - old_money
		
	
	old_money = stats.money

func get_money_per_second():
	var mps = 0.0
	for thing in stats.things:
		if stats.things[thing].count != 0:
			var t = stats.things[thing]
			mps += t.count * (t.base + t.count * t.compound) * t.bonus
	return mps
	

func calculate(delta):
	var total = 0.0
	for thing in stats.things:
		if stats.things[thing].count != 0.0:
			var t = stats.things[thing]
			total = total + t.count * t.base * t.bonus * delta + t.count * t.count * t.compound * t.bonus * delta  
			
	return total

func _ready():
	var data = S.game_load_state()
	hud.DeleteSave.connect("reset", self, "_reset")
	ready_hud(data)
	set_process(true)

func ready_hud(data):
	stats = data[0]
	hud.Shop.prices = data[1]
	
	hud.Shop.stats_init(stats)
	if not data[1].empty():
		hud.Shop.refresh()
	
	hud.Stats.stats_init(stats)
	hud.Upgrades.stats_init(stats)
	hud.Upgrades.clear_duplicates(stats)
	hud.Money.set_text(money_string % stats.money)
	hud.About.set_version(S.major + S.minor+ "\n" + S.code_name)

func _reset():
	S.clear_save()
	stats = S.get_init_stats()
	hud.Shop.set_prices(S.get_init_prices())
	hud.Upgrades.reset()
	hud.Money.set_text(money_string % stats.money)
	hud.Stats.stats_update(stats)

func spawn_dino():
	if randi() % 2 == 0:
		var y = rand_range(11, get_viewport_rect().size.height - 84)
		var pos = Vector2(get_viewport_rect().size.width, y)
		var dino = Dino.instance()
		dino.set_pos(pos)
		dino.direction = "left"
		Dinos.add_child(dino)
	else:
		var y = rand_range(11, get_viewport_rect().size.height - 84)
		var pos = Vector2(0, y)
		var dino = Dino.instance()
		dino.set_pos(pos)
		dino.direction = "right"
		dino.set_flip_h(true)
		Dinos.add_child(dino)

func _on_Egg_pressed():
	var devmode = 1
	
	if dev_mode_on:
		devmode = 100000000
		
	var compound = stats.Clicks.compound * stats.things.Incubators.count
	stats.float_money += (stats.Clicks.base + compound) * stats.Clicks.bonus * devmode
	var fa = FloatAway.instance()
	fa.set_text("click!")
	fa.set_pos(get_global_mouse_pos())
	add_child(fa)
	spawn_dino()
	hud.Egg.get_node("Samples").play("click")
	stats.Clicks.count += 1

func _on_ToggleShop_pressed():
	if not hud.Stats.is_hidden():
		hud.Stats.hide()
	elif not hud.Upgrades.is_hidden():
		hud.Upgrades.hide()
		
	if hud.Shop.is_hidden():
		hud.Shop.show()
	else:
		hud.Shop.hide()

func _on_ToggleUpgrades_pressed():
	if not hud.Shop.is_hidden():
		hud.Shop.hide()
	elif not hud.Stats.is_hidden():
		hud.Stats.hide()
		
	if hud.Upgrades.is_hidden():
		hud.Upgrades.check_for_unlock(stats)
		hud.Upgrades.show()
	else:
		hud.Upgrades.hide()

func _on_ToggleStats_pressed():
	if not hud.Shop.is_hidden():
		hud.Shop.hide()
	elif not hud.Upgrades.is_hidden():
		hud.Upgrades.hide()
		
	if hud.Stats.is_hidden():
		hud.Stats.stats_update(stats)
		hud.Stats.show()
	else:
		hud.Stats.hide()

func _on_Delete_Save_pressed():
	hud.DeleteSave.show()

func _on_About_pressed():
	hud.About.show()

func _on_Music_toggled( pressed ):
	if not pressed:
		get_node("Music").play()
	else:
		get_node("Music").stop()
	pass
