extends Control
var hidden = true
const path = "Main/Content/Upgrades/%s/List"
const nav_path = "Main/Content/Nav/%s"
var cur_page = 0
const pages =\
	[ "Clicks"
	, "Incubators"
	, "Barns"
	, "Workers"
	, "DinoFreightTrucks"
	, "DesignerDinos"
	, "CyberDinos"
	, "Transmutators"
	]
const names =\
	[ "Clicks"
	, "Incubators"
	, "Barns"
	, "Workers"
	, "Dino Freight Trucks"
	, "Designer Dinos"
	, "Cyber Dinos"
	, "Transmutators"
	]
onready var NavLabel = get_node(nav_path % "Label")
onready var Holders =\
	{ Clicks =\
		get_node(path % "Clicks")
	, Incubators =\
		get_node(path % "Incubators")
	, Barns =\
		get_node(path % "Barns")
	, Workers =\
		get_node(path % "Workers")
	, DinoFreightTrucks =\
		get_node(path % "DinoFrieghtTrucks")
	, DesignerDinos =\
		get_node(path % "DesignerDinos")
	, DinoWorkers =\
		get_node(path % "DinoWorkers")
	, DinoDigitizers =\
		get_node(path % "DinoDigitizers")
	, CyberDinos =\
		get_node(path % "CyberDinos")
	, Transmutators =\
		get_node(path % "Transmutators")
	}
const format = "%0.2f$"
class Upgrade:
	var name
	var building
	var effect
	var how_much
	var unlocks_at
	var value
	var tooltip
	var unlocked = false
	func _init(name, effect, how_much, unlocks_at, value, tooltip):
		self.name = name
		self.effect = effect
		self.how_much = how_much
		self.unlocks_at = unlocks_at
		self.value = value
		self.tooltip = tooltip


func mult_bonus(name, how_much, unlocks_at, value, tooltip):
	return Upgrade.new(name, "_multiply_bonus", how_much, unlocks_at, value, tooltip)

func mult_base(name, how_much, unlocks_at, value, tooltip):
	return Upgrade.new(name, "_multiply_base", how_much, unlocks_at, value, tooltip)

func increase_base(name, how_much, unlocks_at, value, tooltip):
	return Upgrade.new(name, "_increase_base", how_much, unlocks_at, value, tooltip) 

func increase_compound(name, how_much, unlocks_at, value, tooltip):
	return Upgrade.new(name, "_increase_compound", how_much, unlocks_at, value, tooltip)

func increase_click_compound(name, building, how_much, unlocks_at, value, tooltip):
	var u = Upgrade.new(name, "_increase_click_compound", how_much, unlocks_at, value, tooltip)
	u.building = building
	return u
# @-@ this most labor intesive part
# So. Many. Names.
# So. Many. Description.
var unlockables =\
	{ Clicks =\
		[ mult_base("Caffeine", 2, 10, 100, "What do you mean 'Caffeine Addicition'?\nDoubles click value")
		, mult_base("Double Expresso", 2, 25, 150, "MORE CAFFEINE!!\nDouble click value")
		, mult_base("Energy Bars", 2, 50, 250, "High in protien, Higher in carbs\nDoubles click value!")
		, mult_base("Double Click", 2, 100, 500, "Ratta Tap Tap!\nDoubles click value.")
		, mult_base("Gaming Mouse", 2, 250, 1000, "Hey now, get your game on!\n")
		, mult_base("Power Clicker", 2, 500, 2000, "CLICKING INTENSIFIES\n")
		, mult_base("Click Bot", 2, 750, 4000, "click click click click cl...\n")
		, mult_base("Click Net", 2, 1000, 8000, "DCOS - Distributed Clicking of Service\n")
		, mult_base("Lightspeed Clicks", 4, 1000, 50000, "That's a whole lot of clicks!\n")
		, mult_base("Click Bait", 4, 2000, 10000, "Number 1.4142135... will shock you!\n")
		, mult_base("Vibrating Fingers", 4, 5000, 20000, "clickclickclickclickcl...\n")
		, increase_compound("Baby Clickers", 1, 250, 2000, "The little ones want to help out!\n")
		, increase_compound("Dino Clicking", 10, 500, 5000, "Dinosaurs love clicker games!")
		, increase_compound("Dynamic Dinos", 25, 1000, 8000, "Clicking up a storm!")
		
		
		]
	, Incubators =\
		[ mult_base("Bigger Incubators", 2, 10, 100, "Cram more eggs in!\nDoubles base rate!")
		, mult_bonus("Smaller Eggs", 2, 50, 500, "Smaller eggs, but bigger dinos!\nDoubles output!")
		, mult_base("Huge Incubators", 2, 100, 1000, "The Bigger the Better!\nDoubles base rate!")
		, mult_bonus("Extra Small Eggs", 2, 100, 2000, "I can't believe it's not a bird!\nDoubles efficiency!!")
		, mult_bonus("Micro Eggs", 2, 1500, 50000, "5000% smaller but still just as durable!\nDoubles out put!")
		, increase_compound("Incubator Incubator", 0.02, 10, 500, "Yo Dog!\n+0.02 base per each Incubator!")
		, increase_compound("Clonenobator", 2, 500, 10000, "Don't count you dinos before they hatch!\n+2 per each incubator")
		, increase_base("Egghouse", 10, 500, 5000, "It's like a warehouse, but for eggs!\nRaise base rate by 10!")
		, increase_base("Pocket Dimensions", 500, 500, 25000, "They're big on the inside than the outside.")
		, increase_base("Higher Dimensions", 1000, 3000, 100000, "Storage so great it can't be comprehended!\nRaise base rate by 1000")
		]
	, Barns =\
		[ mult_bonus("Double Decker Barns", 2, 10, 100, "* maximum weight of 2 tons\nDoubles output!")
		, mult_bonus("Basement Barns", 2, 25, 250, "Complete with a pool table\nMakes Barns twice as good!")
		, mult_bonus("Barn Expansions", 2, 50, 500, "Finally added that patio!\n2x efficiency!")
		, mult_base("Quad Decker Barns", 2, 100, 1000, "High barns yeild higher profits!\nDoubles efficiency!")
		, mult_bonus("Tower Barns", 2, 250, 2500, "No need to defend this tower!\nBarns are twice as efficient!")
		, mult_bonus("City Barns", 2, 500, 5000, "Unfortuantly they don't have any Meteorbuck.\nDouble output!")
		, mult_bonus("Metropolitan Barn Area", 2, 1000, 10000, "Jurassic Urban Sprawl doesn't have the same ring\nDoulbes output")
		, increase_compound("Hotel Barns", 10, 100, 5000, "Dino's pay for their dino stay!\n+10 base per barn!") 
		]
	, Workers =\
		[ mult_bonus("Shorter Lunches", 2, 10, 100, "Just multitask, it's that simple!\nRaises base rate")
		, mult_bonus("Madatory Overtime", 2, 25, 500, "Double the work, have the pay!\nDoubles output!")
		, mult_bonus("Unpaid Interns", 2, 50, 1000, "Labor paid with education!\nDoubles efficiency!")
		, mult_bonus("Mandatory Doubletime", 2, 100, 2000, "GET BACK TO WORK!!!\nDoubles productivity!")
		, mult_bonus("Anti-Union", 2, 250, 4000, "THERE ARE NO BREAKS ON THIS TRAIN!!\nRase base rate!")
		, mult_bonus("Robotic Workers", 2, 500, 8000, "They work fast and hard for 100% free!\nDouble productivity!")
		, increase_base("No Breaks", 100, 250, 5000, "Don't you know? Unions take jobs!\nRaise base rate!")
		, increase_base("Faster Workers", 10, 10, 200, "Work faster or your thrown into the fire.\nDouble output!")
		]
	, DinoFreightTrucks =\
		[ mult_bonus("Fuel Efficient", 2, 10, 2500, "500+ miles per gallon!\nTruck are twice as efficient")
		, mult_bonus("Aerodynamic Trucks", 2, 25, 5000, "Like a hot knife through space!\nDoubles output!")
		, mult_bonus("Self-Driving Vihicle", 2, 50, 10000, "Humans need not apply.\nDouble output!")
		, mult_bonus("Hovering Trucks", 2, 100, 25000, "It's just like air hockey!\nDouble output")
		, mult_bonus("Sonic Trucks", 2, 250, 50000, "Must proceed with haste!\nDoubles productivity")
		, mult_bonus("Super-Sonic Trucks", 2, 500, 100000, "* We are not responsible\nfor any broke glass from sonic boom.\nDouble efficiency")
		, mult_bonus("Warp Trucks", 2, 1000, 250000, "Get from A to Z by skipping B through Y!\nDouble output")
		]
	, DesignerDinos =\
		[ mult_bonus("Glamorous Dinos", 2, 10, 3000, "These be beautiful boy.\nDoubles productivity")
		, mult_bonus("Bigger Dinos", 2, 25, 6000, "Got to go fast!\nDoubles productivity!")
		, mult_bonus("Smarter Dinos", 2, 50, 12000, "Clever girl!\nDouble output")
		, mult_bonus("Faster Dinos", 2, 100, 24000, "Just make sure to stay in the truck\nDesigner Dinos are twice as good")
		, mult_bonus("Stronger Dinos", 2, 250, 48000, "[Insert Draft Punk Joke]\nDoubles efficiency!")
		, mult_bonus("Telepathic Dinos", 2, 500, 96000, "Big Dino always knows what your thinking!\nDoubles efficiency")
		, mult_bonus("Dino Hive Mind", 2, 1000, 192000, "W E  A R E  O N E.  W E  A R E  M A N Y.\nDoubles efficiency!")
		]
	, DinoWorkers =\
		[ mult_bonus("Velociraptor Workers", 2, 10, 5000, "They work at a high... velocity! get it?\nDinoWorkers are twice as efficient")
		, mult_bonus("Stegosaur Workers", 2, 25, 10000, "Slow but strong!\nDoubles efficiency!")
		, mult_bonus("Pterodactyl Workers", 2, 50, 50000, "They'll Pter you a new one.\nDouble efficiency!\nDoubles output!")
		, mult_bonus("Mauisaurus ", 2, 50, 100000, "Not a dinosaur.\nDouble efficiency")
		, mult_bonus("Tyrannosaur Workers", 2, 100, 500000, "Bigger dinos for bigger profit!\nDoubles output!")
		, mult_bonus("Spinosaurus Workers", 2, 250, 1000000, "Another big dino for heavy lifting!\nDouble output!")
		, mult_bonus("Two-Head T-Rexs", 2, 500, 5000000, "While not 100% pratical, it is quite cool!\nDouble output!")
		, mult_bonus("Human Hybrid", 2, 1000, 10000000, "I think it's time to stop.\nDouble productivity!")
		]
	, DinoDigitizers =\
		[ mult_bonus("Loss Less Compression", 2, 10, 20000, "Compression artifacts are a thing of the past!\nDoubles output!")
		, mult_bonus("Defragmentation", 2, 25, 40000, "Keeps dinos in one piece!\nDoubles output!")
		, mult_bonus("High Speed Transfer Cable", 2, 50, 80000, "Helps keep data flowing!\nDoubles output!")
		, mult_bonus("Solid State Storage", 2, 100, 160000, "Faster than a Hard Drive!\nDouble output!")
		, mult_bonus("Parall Digitization", 2, 250, 320000, "Why simple do thing one at a time?\nDouble output!")
		, mult_bonus("Zetabyte Storage Drives", 2, 500, 640000, "You could store almost everything!\nDoubles output!")
		, mult_bonus("Black Hole Storage Drive", 2, 1000, 1280000, "Now you can store everything!\nDouble output!")
		]
	, CyberDinos =\
		[ mult_bonus("Quad Core", 2, 10, 10000, "More cores for better processing!")
		, mult_bonus("Octa Core", 2, 25, 25000, "It's really just 2 Quad Core CPUs glued together!")
		, increase_compound("Multithreading", 100, 50, 50000, "No 'one track mind' on these dinos!")
		, mult_bonus("DDR 10", 2, 50, 100000, "It'll be out of data in a year.\n")
		, mult_bonus("Distributed Computing", 2, 100, 50000, "Why have one computer, when you can have many\nDoubles output!")
		, mult_bonus("80 bit CPU", 2, 250, 50000, "That's a lot more bits than you think!\nDoubles productivity!")
		, mult_bonus("Zetabyte Ram Sticks", 2, 500, 50000, "That's a whole lot of RAM!\nDoubles out put!")
		, mult_bonus("Quantum Processors", 2, 1000, 50000, "60% 1 and 40% 0.\nDoubles out put!")
		]
	, Transmutators =\
		[ mult_bonus("Two For One", 2, 10, 25000, "Equivalent exchange, Equivalent smange.\nDouble productivity!")
		, mult_bonus("Engery Dino Converter", 2, 25, 50000, "M = E/(C^2)\nDoubles productivity!")
		, mult_bonus("Thermodynamics Off", 2, 50, 750000, "Matter can in fact be distroyed and created.\nDoubles out put!")
		, mult_bonus("Quantum Cloning", 2, 100, 1000000, "May or may not violate the rules of reality.\nTransmutators are twice a productive.")
		, mult_bonus("Pan Dimensional Transumation", 2, 250, 50000, "Create dinos across infinite demensions!\nDouble efficiency!")
		, mult_bonus("Anti-Matter Transfusers", 2, 500, 50000, "Harness the power of anti-matter.\nDouble Transmutator productivity!")
		, mult_bonus("Abyssal Condensers", 2, 1000, 50000, "Create dinos from the beyond the abyss.\nDoubles out put!")
		]
}

var stats
func stats_init (stats):
	self.stats = stats
	clear_duplicates(stats)

func is_hidden(): 
	return hidden

func hide():
	hidden = true
	set_pos(Vector2(546,0))

func show():
	hidden = false
	set_pos(Vector2(0,0))
	
# Owned is expects data to be formated
# as owned => { Incubator = [...], Barn = [...], ...}
func clear_duplicates(stats):
	for building_name in stats.upgrades:
		for index in stats.upgrades[building_name]:
			unlockables[building_name][index].unlocked = true

func reset():
	for building in unlockables:
		for unlockable in unlockables[building]:
			unlockable.unlocked = false
			var children = Holders[building].get_children()
			for child in children:
				Holders[building].remove_child(child)
				child.free()
			
func check_for_unlock(stats):
	for building in unlockables:
		var current_thing
		if building == "Clicks":
			current_thing = stats.Clicks
		else:
			current_thing = stats.things[building]
		var index = 0
		for unlockable in unlockables[building]:
			if current_thing.count >= unlockable.unlocks_at\
			and not unlockable.unlocked:
				var button = Button.new()
				var container = HBoxContainer.new()
				var label = Label.new()
				
				label.set_text(format % unlockable.value)
				
				button.set_text(unlockable.name)
				button.set_tooltip(unlockable.tooltip)
				unlockable.building = building
				if unlockable.effect != "_increase_click_compound":
					button.connect("pressed", self, unlockable.effect, [unlockable, index, button, container])
				else:
					button.connect("pressed", self, unlockable.effect, [unlockable, index, button, container])
				container.add_child(button)
				container.add_child(label)
				
				Holders[building].add_child(container)
				unlockable.unlocked = true
				index += 1
				# You know you nested to deep when... 
			# end
		# end
	# end
# end

func _multiply_bonus(item, index, button, container):
	var thing
	if item.building == "Clicks":
		thing = stats.Clicks
	else:
		thing = stats.things[item.building]
	if stats.money <= item.value: 
		return
	stats.things[item.building].bonus *= item.how_much
	stats.upgrades[item.building].append(index)
	stats.money -= item.value
	remove_upgrade(item.building, button, container, "_multiply_bonus")
	

func _multiply_base(item, index, button, container):
	var thing
	if item.building == "Clicks":
		thing = stats.Clicks
	else:
		thing = stats.things[item.building]
		
	if stats.money <= item.value: 
		return
	thing.base *= item.how_much
	stats.upgrades[item.building].append(index)
	stats.money -= item.value
	remove_upgrade(item.building, button, container, "_multiply_base")
	
func _increase_base(item, index, button, container):
	var thing
	if item.building == "Clicks":
		thing = stats.Clicks
	else:
		thing = stats.things[item.building]
		
	if stats.money <= item.value: 
		return
	thing.base += item.how_much
	stats.upgrades[item.building].append(index)
	stats.money -= item.value
	remove_upgrade(item.building, button, container, "_increase_base")

func _increase_compound(item, index, button, container):
	var thing
	if item.building == "Clicks":
		thing = stats.Clicks
	else:
		thing = stats.things[item.building]
		
	if stats.money <= item.value: 
		return
	thing.base += item.how_much
	stats.upgrades[item.building].append(index)
	stats.money -= item.value
	remove_upgrade(item.building, button, container, "_increase_compound")

func _increase_click_compound(item, index, button, container):
	stats.click.compound[item.building] += item.how_much
	stats.upgrades[item.building].append(index)
	stats.money -= item.value
	remove_upgrade(item.building, button, container, "_increase_compound")

func remove_upgrade(building, button, container, disconnect):
	button.disconnect("pressed", self, disconnect)
	Holders[building].remove_child(container)
	container.free()
	
func _ready():
	NavLabel.set_text(names[cur_page])
	Holders[pages[cur_page]].get_parent().show()
	pass


func _on_Prev_pressed():
	Holders[pages[cur_page]].get_parent().hide()
	cur_page -= 1
	if cur_page < 0:
		cur_page = pages.size() - 1
	NavLabel.set_text(names[cur_page])
	Holders[pages[cur_page]].get_parent().show()
	pass


func _on_Next_pressed():
	Holders[pages[cur_page]].get_parent().hide()
	cur_page += 1
	cur_page %= pages.size()
	NavLabel.set_text(names[cur_page])
	Holders[pages[cur_page]].get_parent().show()
