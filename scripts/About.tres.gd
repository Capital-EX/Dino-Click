extends Popup


func _ready():
	pass

func set_text(string):
	get_node("Panel/Container/Label").set_text(string)

func set_version(string):
	get_node("Panel/Container/Version").set_text(string)

func _on_Yes_pressed():
	hide()
	pass