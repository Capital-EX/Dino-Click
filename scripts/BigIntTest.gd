extends Node2D
const BigDecimal = preload("res://scripts/BigDecimal.gd")



func _ready():
	var a = BigDecimal.from_list([250, 1])
	var b = BigDecimal.from_list([250, 1])
	print(a.to_string(), " + ", b.to_string(), " = ", a.plus(b).to_string())
	print(a.times(b).digits)
	pass
