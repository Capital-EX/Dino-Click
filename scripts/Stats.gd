extends Control
onready var StatList = get_node("Panel/StatList/StatContainer")
const building_label = "Number of %ss owned: %d"
const total_label = "Total number of buildings: %d"
const click_label = "Number of clicks made: %d"
var labels = {}
var hidden = true
var stats

func is_hidden(): 
	return hidden

func hide():
	hidden = true
	set_pos(Vector2(-546,0))

func show():
	hidden = false
	set_pos(Vector2(0,0))

func stats_init(stats):
	var total = 0
	var l = Label.new()
	l.set_text(click_label % stats.Clicks.count)
	labels.Clicks = l
	StatList.add_child(l)

	for thing in stats.things:
		l = Label.new()
		l.set_text(building_label % [stats.things[thing].name, stats.things[thing].count])
		total += stats.things[thing].count
		labels[thing] = l
		StatList.add_child(l)
	# l = Label.new()
	# l.set_text(total_label % total)
	# StatList.add_child(l)

func stats_update(stats):
	labels.Clicks.set_text(click_label % stats.Clicks.count) 
	
	for thing in stats.things:
		labels[thing].set_text(building_label % [stats.things[thing].name, stats.things[thing].count])

func _ready():
	set_process(true)
	pass
