extends Label

const speed = -0.5
const sway = 10
const life = 3
var start_x
var life_time = 0
var phase

func _process(delta):
	life_time += delta
	var new_pos = Vector2(start_x + sway * sin((life_time + phase) * 10 ), get_pos().y + -delta)
	
	set_pos(new_pos)
	if life_time > life:
		queue_free()

func _ready():
	phase = randf() * 2 * PI
	start_x = get_pos().x
	set_process(true)
	pass
