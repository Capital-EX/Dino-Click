extends Control
onready var Upgrades = get_node("../Upgrades")

var hidden = true
var stats
var prices

const unlocks =\
	[ [10000000, "Transmutators"]
	, [1000000,"CyberDinos"]
	, [500000, "DinoDigitizers"]
	, [100000, "DinoWorkers"]
	, [50000, "DesignerDinos"]
	, [10000, "DinoFreightTrucks"]
	, [1000, "Workers"]
	, [500, "Barns"]
	]

const path = "Panel/Container/Items/%s"

onready var items =\
	{ Incubators =\
		get_node(path % "Incubators")
	, Barns =\
		get_node(path %  "Barns")
	, Workers =\
		get_node(path % "Workers")
	, DinoFreightTrucks =\
		get_node(path % "DinoFreightTrucks")
	, DesignerDinos =\
		get_node(path % "DesignerDinos")
	, DinoWorkers =\
		get_node(path % "DinoWorkers")
	, DinoDigitizers =\
		get_node(path % "DinoDigitizers")
	, CyberDinos =\
		get_node(path % "CyberDinos")
	, Transmutators =\
		get_node(path % "Transmutators")
	}

func is_hidden(): 
	return hidden
	
func set_hidden(state): 
	hidden = state

func stats_init (stats):
	self.stats = stats
	
func check_for_unlocks(money):
	while unlocks.size() != 0 and money > unlocks[unlocks.size() - 1][0]:
		unlock(unlocks.back()[1])
		unlocks.pop_back()

func hide():
	hidden = true
	set_pos(Vector2(0,-483))
	
func show():
	hidden = false
	set_pos(Vector2(0,0))

func unlock(thing):
	items[thing].show()

func refresh():
	for item in prices:
		update_shop(item, prices[item])

func update_shop(thing, cost):
		if items.has(thing):
			items[thing].get_node("Price").set_text("%0.2f" % cost)

func _ready():
	for item_name in items:
		items[item_name].get_node("Buy").connect("pressed", self, "buy", [item_name])
		items[item_name].get_node("Buy10").connect("pressed", self, "buy10", [item_name])
	pass
	


func buy(thing):
	if stats.money > prices[thing]:
		stats.money -= prices[thing]
		stats.things[thing].count += 1
		prices[thing] += 0.06 * prices[thing]
		update_shop(thing, prices[thing])

func buy10(thing):
	var buys = 0
	while buys < 10 and stats.money >= prices[thing]:
		buys += 1
		stats.money -= prices[thing]
		stats.things[thing].count += 1
		prices[thing] += 0.06 * prices[thing]
	update_shop(thing, prices[thing])

func set_prices(p):
	prices = p
	refresh()