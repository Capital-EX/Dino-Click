const major = "0.20"
const minor = "b"
const code_name = "Unstable Saves"
const init_stats =\
	{ money = 0
	, float_money = 0.0
	, Clicks =\
		{ name = "Clicks", count = 0, base = 1, bonus = 1, compound = 0}
	# Dict => (Dict => Key -> Value)
	, things =\
		{ Incubators =\
			{name = "Incubators", count = 0, base = 1, bonus = 1, compound = 0}
		, Barns =\
			{name = "Barns", count = 0, base = 25, bonus = 1, compound = 0}
		, Workers =\
			{name = "Workers", count = 0, base = 100, bonus = 1, compound = 0}
		, DinoFreightTrucks =\
			{name = "Dino Freight Trucks", count = 0, base = 250, bonus = 1, compound = 0}
		, DesignerDinos =\
			{name = "Designer Dinos", count = 0, base = 500, bonus = 1, compound = 0}
		, DinoWorkers =\
			{name = "Dino Workers", count = 0, base = 1000, bonus = 1, compound = 0}
		, DinoDigitizers =\
			{name = "Dino Digitizers", count = 0, base = 5000, bonus = 1, compound = 0}
		, CyberDinos =\
			{name = "Cyber Dinos", count = 0, base = 10000, bonus = 1, compound = 0}
		, Transmutators =\
			{name = "Transmutators", count = 0, base = 50000, bonus = 1, compound = 0}
		}
	# Dict => Key -> [String]
	, upgrades =\
		{ Incubators = []
		, Barns = []
		, Workers = []
		, DinoFreightTrucks = []
		, DesignerDinos = []
		, DinoWorkers = []
		, DinoDigitizers = []
		, CyberDinos = []
		, Transmutators = []
		, Clicks = []
		}
	, science = {}
}

const init_prices =\
	{ Incubators = 10
	, Barns = 500
	, Workers = 1000
	, DinoFreightTrucks = 10000
	, DesignerDinos = 50000
	, DinoWorkers = 100000
	, DinoDigitizers = 500000
	, CyberDinos = 1000000
	, Transmutators = 10000000
	}

static func deep_copy(source, dest):
	for key in source:
		if typeof(source[key]) == TYPE_DICTIONARY:
			var to
			if dest.has(key):
				 to = dest[key] 
			else:
				to = {}
			dest[key] = deep_copy(source[key], to)
			
		elif typeof(source[key]) == TYPE_ARRAY:
			var copy = []
			for item in source[key]:
				if typeof(item) == TYPE_DICTIONARY:
					copy.append(deep_copy(item, {}))
				elif typeof(item) == TYPE_ARRAY:
					#TODO: finish me...
					pass
				else:
					copy.append(item)
			dest[key] = copy
		else:
			dest[key] = source[key]
	return dest

static func game_load_state():
	var save = File.new()
	var loaded_stats = deep_copy(init_stats, {})
	var loaded_prices = deep_copy(init_prices, {})
	
	if not save.file_exists("user://savegame.save"):
		return [loaded_stats, loaded_prices]
		
	save.open("user://savegame.save", File.READ)
	var loaded_major_json  =\
		save.get_line()
	save.get_line() # ignore minor
	save.get_line() # ignore code name
	var loaded_stats_json =\
		save.get_line()
	var loaded_prices_json =\
		save.get_line()
	save.close()
	
	if loaded_major_json != major:
		return [loaded_stats, loaded_prices]
	
	if not loaded_stats_json.empty():
		loaded_stats = {}
		loaded_stats.parse_json(loaded_stats_json)
	
	if not loaded_prices_json.empty():
		loaded_prices = {}
		loaded_prices.parse_json(loaded_prices_json)
	
	return [loaded_stats, loaded_prices]

static func game_save_state(stats, prices):
	var save = File.new()
	save.open("user://savegame.save", File.WRITE)
	save.store_line(major)
	save.store_line(minor)
	save.store_line(code_name)
	save.store_line(stats.to_json())
	save.store_line(prices.to_json())
	save.close()

static func clear_save():
	game_save_state(init_stats, init_prices)
	

static func get_init_stats():
	return deep_copy(init_stats, {})

static func get_init_prices():
	return deep_copy(init_prices, {}) 