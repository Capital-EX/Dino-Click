extends Popup
signal reset

func _ready():
	pass



func _on_Yes_pressed():
	hide()
	var save = File.new()
	if save.file_exists("user://savegame.save"):
		save.open("user://savegame.save", save.WRITE)
		save.close()
	emit_signal("reset")
	pass


func _on_No_pressed():
	hide()
	pass
