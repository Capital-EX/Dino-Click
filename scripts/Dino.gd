extends AnimatedSprite
var state = "running"
var standing_time = 0
var direction = "left"
var speed = 100
var distance_ran = 0

func rand_angle():
	return randf() * 2 * PI

func run(delta):
	distance_ran += delta * speed
	var dir = -1 if direction == "left" else 1
	var pos = get_pos() + Vector2(speed * dir * delta, 0)
	set_pos(pos)
	var tired = rand_range(100, 600)
	if distance_ran > tired:
		distance_ran = 0
		state = "standing"
		play("idle")
	
	if direction == "right":
		if pos.x > get_viewport_rect().size.width + 11:
			queue_free()
	else:
		if pos.x < -11:
			queue_free()
	
	
func stand(delta):
	standing_time += delta
	var thresh = randi() % 2 + 2
	if standing_time > thresh:
		standing_time = 0
		state = "running"
		play("run")
	pass
	

	
func _process(delta):
	if state == "standing":
		stand(delta)
	else:
		run(delta)
	
func _ready():
	play("run")
	set_process(true)
	pass
